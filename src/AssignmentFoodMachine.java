import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AssignmentFoodMachine {
    private int totalPrice = 0;
    private boolean isSetDiscountApplied = false;
    void order(String food,int price) {

        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " +food+ "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

         if (confirmation == 0) {

             boolean hasHamBurger = receivedInfo.getText().contains("hamburger");
             boolean hasCheeseBurger = receivedInfo.getText().contains("cheeseburger");
             boolean hasTeriyakiBurger = receivedInfo.getText().contains("teriyakiburger");
             boolean hasPotato = receivedInfo.getText().contains("frenchFries");
             boolean hasCoffee = receivedInfo.getText().contains("coffee");

             if ((hasHamBurger || hasCheeseBurger || hasTeriyakiBurger) && hasPotato && hasCoffee && !isSetDiscountApplied) {
                 totalPrice -= 100;
                 isSetDiscountApplied = true;

                 String currentText = receivedInfo.getText();
                 receivedInfo.setText(currentText + "discount -100yen\n");

             }

             String currentText = receivedInfo.getText();
             receivedInfo.setText(currentText + food + " " + price + "yen\n");

             totalPrice += price;
             Totallabel.setText("Total: " + totalPrice + " yen");

             JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "! It will be served as soon as possible.");
         }
    }

    private JLabel toplabel;
    private JButton hamburgerButton;
    private JButton teriyakiburgerButton;
    private JButton cheeseburgerButton;
    private JButton frenchFriesButton;
    private JButton coffeeButton;
    private JButton saladButton;
    private JLabel orderlabel;
    private JButton checkout;
    private JLabel Totallabel;
    private JTextPane receivedInfo;
    private JPanel root;

    private void checkout() {
        int paymentConfirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to check out?",
                "Checkout Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if (paymentConfirmation == JOptionPane.YES_OPTION) {
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you!! The total price is " + totalPrice + " yen."
            );

            if (isSetDiscountApplied) {
                totalPrice -= 100;

                receivedInfo.setText("");

                totalPrice = 0;
                Totallabel.setText("Total: 0 yen");
            }
        }
    }

    public AssignmentFoodMachine() {
        hamburgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("hamburger",240);
            }
        });

        hamburgerButton.setIcon(new ImageIcon(
                this.getClass().getResource("/image/hamburger.jpg")
        ));

        cheeseburgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("cheeseburger",280);
            }
        });

        cheeseburgerButton.setIcon(new ImageIcon(
                this.getClass().getResource("/image/cheeseburger.jpg")
        ));


        teriyakiburgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("teriyakiburger",430);
            }
        });

        teriyakiburgerButton.setIcon(new ImageIcon(
                this.getClass().getResource("/image/teriyakiburger.jpg")
        ));


        frenchFriesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("frenchFries",250);
            }
        });

        frenchFriesButton.setIcon(new ImageIcon(
                this.getClass().getResource("/image/frenchFries.jpg")
        ));

        saladButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("salad",300);
            }
        });

        saladButton.setIcon(new ImageIcon(
                this.getClass().getResource("/image/salad.jpg")
        ));

        coffeeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("coffee",260);
            }
        });

        coffeeButton.setIcon(new ImageIcon(
                this.getClass().getResource("/image/coffee.jpg")
        ));

        checkout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkout();
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("AssignmentFoodMachine");
        frame.setContentPane(new AssignmentFoodMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
